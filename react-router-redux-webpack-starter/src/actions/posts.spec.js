import configureMockStore from 'redux-mock-store';
import fetchMock  from 'fetch-mock';
import thunk from 'redux-thunk';
import { expect } from 'chai';
import { fetchPosts } from './posts';
import { ROOT_URL } from '../constants/api.constants';
import { FETCH_POSTS_PROGRESS, FETCH_POSTS_SUCCESS } from './posts';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Posts actions', () => {

    const responseBody = { "page": 1, "per_page": 3, "total": 12, "total_pages": 4, "data": [{ "id": 1, "name": "cerulean", "year": 2000, "pantone_value": "15-4020" }, { "id": 2, "name": "fuchsia rose", "year": 2001, "pantone_value": "17-2031" }, { "id": 3, "name": "true red", "year": 2002, "pantone_value": "19-1664" }] };

    afterEach(() => {
        fetchMock.restore();
    });

    it(`Creates action ${FETCH_POSTS_PROGRESS} then ${FETCH_POSTS_SUCCESS} when fetching posts`, () => {

        const store = mockStore({ posts: {} });

        const expectedActions = [
            { type: FETCH_POSTS_PROGRESS, payload: true },
            { type: FETCH_POSTS_PROGRESS, payload: false },
            { type: FETCH_POSTS_SUCCESS, payload: responseBody.data }
        ];

        fetchMock.get(`${ROOT_URL}/posts`, responseBody);

        return store.dispatch(fetchPosts()).then(() => {
            expect(store.getActions()).to.deep.equal(expectedActions);
        });
    });

});