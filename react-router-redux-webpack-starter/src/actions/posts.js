import { ROOT_URL } from '../constants/api.constants';

export const FETCH_POST_PROGRESS = 'FETCH_POST_PROGRESS';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_ERROR = 'FETCH_POST_ERROR';

export const FETCH_POSTS_PROGRESS = 'FETCH_POSTS_PROGRESS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';

const fetchPostProgress = payload => ({ type: FETCH_POST_PROGRESS, payload });
const fetchPostSuccess = payload => ({ type: FETCH_POST_SUCCESS, payload });
const fetchPostError = payload => ({ type: FETCH_POST_ERROR, payload });

const fetchPostsProgress = payload => ({ type: FETCH_POSTS_PROGRESS, payload });
const fetchPostsSuccess = payload => ({ type: FETCH_POSTS_SUCCESS, payload });
const fetchPostsError = payload => ({ type: FETCH_POSTS_ERROR, payload });

export const fetchPost = (id) => {
    const url = `${ROOT_URL}/posts/${id}`;
    return dispatch => {
        dispatch(fetchPostProgress(true));
        return fetch(url)
            .then(response => {
                dispatch(fetchPostProgress(false));
                if (!response.ok) throw Error(response.statusText);
                return response;
            })
            .then(response => response.json())
            .then(post => dispatch(fetchPostSuccess(post.data)))
            .catch(error => dispatch(fetchPostError({ hasError: true, data: error })));
    };
}

export const fetchPosts = () => {
    const url = `${ROOT_URL}/posts`;
    return dispatch => {
        dispatch(fetchPostsProgress(true));
        return fetch(url)
            .then(response => {
                dispatch(fetchPostsProgress(false));
                if (!response.ok) throw Error(response.statusText);
                return response;
            })
            .then(response => response.json())
            .then(posts => dispatch(fetchPostsSuccess(posts.data)))
            .catch(error => dispatch(fetchPostsError({ hasError: true, data: error })));
    };
}
