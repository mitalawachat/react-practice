import { combineReducers } from 'redux';
import { posts, postLoading, postLoadingError, postsLoading, postsLoadingError } from './posts';

export default combineReducers({  
    postLoading, postLoadingError,
    posts, postsLoading, postsLoadingError
});