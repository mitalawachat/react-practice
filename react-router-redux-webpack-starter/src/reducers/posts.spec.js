import { expect } from 'chai';
import { posts } from './posts';
import { FETCH_POST_SUCCESS } from '../actions/posts';

describe('Posts reducer', () => {
    it('Should return initial state', () => {
        expect(posts(undefined, {})).to.deep.equal({});
    });

    it(`Should handle ${FETCH_POST_SUCCESS} action`, () => {
        const payload = { "id": 1, "name": "cerulean", "year": 2000, "pantone_value": "15-4020" };
        const action = { type: FETCH_POST_SUCCESS, payload };
        const response = posts({}, action);
        const expected = { [payload.id]: payload };
        expect(response).to.deep.equal(expected);
    });
});