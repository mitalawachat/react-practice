import _ from 'lodash';
import { FETCH_POST_SUCCESS, FETCH_POST_PROGRESS, FETCH_POST_ERROR } from '../actions/posts';
import { FETCH_POSTS_SUCCESS, FETCH_POSTS_PROGRESS, FETCH_POSTS_ERROR } from '../actions/posts';

export const posts = (state = {}, action) => {
    switch(action.type) {
        case FETCH_POST_SUCCESS:
            return { ...state, [action.payload.id]: action.payload };
        case FETCH_POSTS_SUCCESS:
            return _.mapKeys(action.payload, 'id');
        default:
            return state;
    }
}

export const postsLoading = (state = false, action) => {
    switch(action.type) {
        case FETCH_POSTS_PROGRESS:
            return action.payload;
        default:
            return state;
    }
}

export const postsLoadingError = (state = {}, action) => {
    switch(action.type) {
        case FETCH_POSTS_ERROR:
            return action.payload;
        default:
            return state;
    }
}

export const postLoading = (state = false, action) => {
    switch(action.type) {
        case FETCH_POST_PROGRESS:
            return action.payload;
        default:
            return state;
    }
}

export const postLoadingError = (state = {}, action) => {
    switch(action.type) {
        case FETCH_POST_ERROR:
            return action.payload;
        default:
            return state;
    }
}