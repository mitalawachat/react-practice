import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import store from './store';

import App from './components/app';
import Post from './components/post/post';
import Posts from './components/posts/posts';

render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route path="/posts/:id" component={Post} />
                <Route path="/posts" component={Posts} />
                <Route path="/" component={App} />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('app')
);
