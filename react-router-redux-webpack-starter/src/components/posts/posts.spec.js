import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import Posts from './posts';

const mockStore = configureMockStore([]);
const store = mockStore({});
const container = shallow(<Provider store={store}><Posts /></Provider>);

describe('<Posts> component', () => {

  it('Should render component', () => {
    expect(container.find(Posts)).to.exist;
  });

});