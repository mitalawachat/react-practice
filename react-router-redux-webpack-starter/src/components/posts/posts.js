import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPosts } from '../../actions/posts';
import './posts.scss';

class Posts extends React.Component {

  componentDidMount() {
    this.props.fetchPosts();
  }

  renderPost(post) {
    return (
      <li key={post.id}>
        <Link to={`/posts/${post.id}`}>{post.name}</Link>
      </li>
    );
  }

  renderPosts(posts) {
    return (
      <ul>
        {posts.map(post => this.renderPost(post))}
      </ul>
    );
  }

  render() {
    const { posts, postsLoading, postsLoadingError } = this.props;

    if (postsLoading)
      return <p>Loading posts...</p>;

    if (postsLoadingError.hasError)
      return <p>Sorry, an error occured! Error: {JSON.stringify(postsLoadingError)}</p>;

    if (posts.length === 0)
      return <p>No posts found!</p>;
    else
      return this.renderPosts(posts);
  }
}

Posts.propTypes = {
  posts: PropTypes.array.isRequired,
  postsLoading: PropTypes.bool.isRequired,
  postsLoadingError: PropTypes.object.isRequired,
  fetchPosts: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    posts: _.values(state.posts),
    postsLoading: state.postsLoading,
    postsLoadingError: state.postsLoadingError
  };
}

export default connect(mapStateToProps, { fetchPosts })(Posts);
