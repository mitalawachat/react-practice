import React from 'react';
import { Link } from 'react-router-dom';
import './app.scss';
import pandaImage from './panda.jpg';

const App = () => (
  <div>
    <Link to="/posts">Posts</Link>
    <br />
    <img src={pandaImage} />
  </div>
);

export default App;