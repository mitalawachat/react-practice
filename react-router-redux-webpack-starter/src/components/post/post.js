import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPost } from '../../actions/posts';
import './post.scss';

class Post extends React.Component {

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.fetchPost(id);
    // TODO: Not getting called when changing routes for the same component, componentWillUpdate and componentWillReceiveProps do
  }

  render() {
    const { post, postLoading, postLoadingError } = this.props;

    if (postLoading)
      return <p>Loading post...</p>;

    if (postLoadingError.hasError)
      return <p>Sorry, an error occured! Error: {JSON.stringify(postLoadingError)}</p>;

    if(post)
      return (
        <div>
          <li>ID : {post.id}</li>
          <li>Name : {post.name}</li>
          <li>Year : {post.year}</li>
        </div>
      );
    else
      return <p>No content!</p>;
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  return {
    post: state.posts[id] ? state.posts[id] : null,
    postLoading: state.postLoading,
    postLoadingError: state.postLoadingError
  };
}

Post.propTypes = {
  match: PropTypes.object.isRequired, // injected by react-router
  post: PropTypes.object.isRequired,
  postLoading: PropTypes.bool.isRequired,
  postLoadingError: PropTypes.object.isRequired,
  fetchPost: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { fetchPost })(Post);