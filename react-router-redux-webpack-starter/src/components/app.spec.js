import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import App from './app';

const container = shallow(<App />);

describe('<App> component', () => {

  it('Should render component', () => {
    expect(container.find(App)).to.exist;
  });

});