import webpack from 'webpack';
import config from './webpack.config.babel';

config.plugins = config.plugins || [];
config.plugins.push(new webpack.optimize.UglifyJsPlugin());
config.plugins.push(new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production')
}));
config.devtool = 'source-map'

module.exports = config;
